# This is a simple example of an application that makes an API call and converts the response into Prometheus-compatible metrics.

To retrieve the International Space Station (ISS) current location from the Open Notify API and expose it as a metric in Prometheus format, you can use Python along with the requests library to make the API request and the prometheus_client library to expose the metric.

## How to run
* Clone repo git clone https://gitlab.com/oangit/prometheus-client-api-request.git
* Go to clone repo cd ./prometheus-client-api-request
* Creating virtual environments is done by running the command "venv".
```bash
python3 -m venv venv
```
* A virtual environment may be “activated” using a script in its binary directory (bin on POSIX)
```bash
source ./venv/bin/activate
```
* Install requirements
```bash
python -m pip install -r requirements.txt
```
* Start app
```
./venv/bin/python ./iss_location.py
```

## Prometheus metrics
This script will continuously fetch the ISS location from the Open Notify API and expose it as Prometheus metrics on port 8080. You can then scrape these metrics using Prometheus or visualize them using Grafana.

To check, open the Terminal and enter
```bash
curl http://127.0.0.1:8080/metrics
```
