import time
import requests
from prometheus_client import start_http_server, Gauge

# Define a Prometheus Gauge metric for ISS location
iss_latitude = Gauge('iss_latitude', 'Current latitude of the ISS')
iss_longitude = Gauge('iss_longitude', 'Current longitude of the ISS')

def fetch_iss_location():
    try:
        response = requests.get('http://api.open-notify.org/iss-now.json')
        data = response.json()
        latitude = float(data['iss_position']['latitude'])
        longitude = float(data['iss_position']['longitude'])
        iss_latitude.set(latitude)
        iss_longitude.set(longitude)
        print(f"ISS Location: Latitude: {latitude}, Longitude: {longitude}")
    except Exception as e:
        print(f"Error fetching ISS location: {e}")

if __name__ == '__main__':
    # Start a Prometheus HTTP server on port 8080
    start_http_server(8080)

    # Periodically fetch ISS location and update Prometheus metrics
    while True:
        fetch_iss_location()
        time.sleep(60)  # Fetch location every 60 seconds
